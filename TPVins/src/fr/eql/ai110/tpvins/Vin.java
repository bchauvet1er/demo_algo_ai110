package fr.eql.ai110.tpvins;

public class Vin {

	private String 	nom;
	private String 	appellation;
	private String 	region;
	private String 	producteur;
	private String	surface;
	private String 	numeroStand;
	
	public Vin() {
		
	}
	
	public Vin(String nom, String appellation, String region, String producteur, String surface, String numeroStand) {
		this.nom = nom;
		this.appellation = appellation;
		this.region = region;
		this.producteur = producteur;
		this.surface = surface;
		this.numeroStand = numeroStand;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAppellation() {
		return appellation;
	}
	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getProprio() {
		return producteur;
	}
	public void setProprio(String producteur) {
		this.producteur = producteur;
	}
	public String getSurface() {
		return surface;
	}
	public void setSurface(String surface) {
		this.surface = surface;
	}
	public String getNumeroStand() {
		return numeroStand;
	}
	public void setNumeroStand(String numeroStand) {
		this.numeroStand = numeroStand;
	}

	@Override
	public String toString() {
		return "Vin [nom=" + nom.trim() + ", appellation=" + appellation.trim() + ", region=" + region.trim() + ", producteur=" + producteur.trim()
				+ ", surface=" + surface.trim() + ", numeroStand=" + numeroStand.trim() + "]";
	}
	
	
}
