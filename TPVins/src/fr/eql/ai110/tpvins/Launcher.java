package fr.eql.ai110.tpvins;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Launcher {

	public static void main(String[] args) throws IOException {

		WineManager wm = new WineManager();

		// pr�paration du fichier tri� : 
		wm.formatRafData();
		
		
		Scanner scan = new Scanner(System.in);
		
		while(true)
		{
			System.out.print("Vin � rechercher : ");
		
			String nom = scan.nextLine();
		
			Vin v = wm.searchByName(nom);
		
			if (v != null) {
				System.out.println(v);
			}
			else
			{
				System.out.println("Vin non trouv�...");
			}
			
		}
	}

}
