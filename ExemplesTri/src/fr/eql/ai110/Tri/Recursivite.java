package fr.eql.ai110.Tri;

public class Recursivite {
	
	public static void main(String[] args) {
		exemple(5);
		
		System.out.println("YOUHOU");
		
		exemple(25);
		
		long resultat = factorielle(2);
	}
	
	public static long factorielle(long x) {
		
		if (x < 2) {
			return 1;
		}
		else {
			return x * factorielle(x - 1);
		}
	}
	
	public static void exemple(int nbPassages) {	
		System.out.println("plop");
		//nbPassages--;
		if (nbPassages > 0) {			
			exemple(nbPassages - 1);
		}
	}

}
