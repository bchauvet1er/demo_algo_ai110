package fr.eql.ai110.mastermind;

import java.util.Random;
import java.util.Scanner;

public class MastermindManager {

	private final static char[] AVAILABLE_COLORS = { 'b', 'j', 'r', 'v', 'm', 'n' };
	private int combinationLength = 4;
	private int maxNumberOfRounds = 10;

	private int roundNumber = 0;
	private int placedItemsCount = 0; // right color at the right place
	private int foundItemsCount = 0; // right color at the wrong place
	private char[] solution;
	private char[][] history;

	// passer playerChoice en variable locale
	private char[] playerChoice;

	public void initialize() {
		this.solution = new char[combinationLength];
		this.history = new char[maxNumberOfRounds][combinationLength];
		this.roundNumber = 0;
		this.placedItemsCount = 0;
		this.foundItemsCount = 0;

		generateSolution();
	}

	private void generateSolution() {
		Random rnd = new Random();
		
		for (int i=0; i < combinationLength; i++) {
			int x = rnd.nextInt(AVAILABLE_COLORS.length);
			
			solution[i] = AVAILABLE_COLORS[x];
		}
		
		System.out.print("LA solution : ");
		for (int i=0; i < combinationLength; i++) {
			System.out.print(solution[i]);
		}
		System.out.println();
		
	}

	public void playGame() {

		initialize();

		// r�p�ter
		do {
			playRound();
		} while (!gameIsOver());

		// fin de partie : afficher le r�sultat de la partie
		displayGameResult();

		// TODO proposer nouvelle partie
	}

	private boolean gameIsOver() {
		boolean allItemsArePlaced = placedItemsCount == combinationLength;
		boolean allRoundsArePlayed = roundNumber == maxNumberOfRounds;

		return allItemsArePlaced || allRoundsArePlayed;
	}

	private void displayGameResult() {
		// impl�menter displayGameResult
		if (placedItemsCount == combinationLength) {
			System.out.println("GAGNEEEE !!!");
		}
		else {
			System.out.println("Perdu... ");
		}
	}

	private void playRound() {
		
		// r�cup�rer le choix du joueur
		getPlayerChoice();
		// TODO v�rifier la validit� du choix
		// comparer avec la solution
		compareChoiceWithSolution();
		// afficher le r�sultat du round
		displayRoundResult();
		// TODO mettre � jour l'historique
	}

	private void displayRoundResult() {
		System.out.println("Plac�s : " + placedItemsCount + " - Trouv�s : " + foundItemsCount);
	}

	private void getPlayerChoice() {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Votre combinaison ?");
		playerChoice = scan.nextLine().toCharArray();	
	}

	private void compareChoiceWithSolution() {
		placedItemsCount = 0;
		foundItemsCount = 0;

		char[] tmpSolution = new char[combinationLength];
		
		for (int i = 0; i < combinationLength; i++) {
			tmpSolution[i] = solution[i];
		}
		
		calculatePlacedItems(tmpSolution);

		calculateFoundItems(tmpSolution);

	}

	private void calculateFoundItems(char[] solution) {
		for (int i = 0; i < combinationLength; i++) {
			// v�rifier le nombre de trouv�s
			for (int j = 0; j < combinationLength; j++) {
				if (i != j) {
					if (playerChoice[j] == solution[i]) {
						foundItemsCount++;
						playerChoice[j] = '-';
						solution[i] = '*';
					}
				}
			}
		}
	}

	private void calculatePlacedItems(char[] solution) {
		// v�rifier le nombre de plac�s
		for (int i = 0; i < combinationLength; i++) {
			if (playerChoice[i] == solution[i]) {
				placedItemsCount++;
				playerChoice[i] = '-';
				solution[i] = '*';
			}
		}
	}

}
